//Modules includes
const mongoose = require("mongoose");
//!---



//Create Model
const Task = mongoose.model("task",
    {
        description:
            {
                type:String,
                required : true,
                trim : true
            },
        completed: {
            type: Boolean,
            default : false
        },

        owner : {
            type : mongoose.Schema.Types.ObjectId,
            required : true,
            ref : "user"
        }
    });




//Module Export

module.exports = Task;