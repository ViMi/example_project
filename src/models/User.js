//Modules includes
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const secret_key = "kdweueksdsjkij";


const isEmail = require("../utils/validate/isEmail");



//!---


let userSchema = new mongoose.Schema({
    name:{
        type:String,
        required : true,
        trim : true
    },
    email: {
            type:String,
            validate(value)
            {
                if(!isEmail(value))
                    throw new Error("Error: Email is invalid");
            },
            lowercase: true,
            unique : true,
            index:true
        },
    password: {
            type : String,
            required : true,
            trim : true,
            // match: "[^password]",
            minlength : 7
        },
    age: {
        type: Number,
        validate(value) {
            if(value < 0)
                throw new Error("Age must be a positive number");
        },

        default : 0

    },
    tokens: [{
        token : {
            type : String,
            required : true
        }
    }]
},

    {

        toJSON: { virtuals : true},
       toObject: { virtuals : true}
    }
);


userSchema.virtual("tasks", {
    ref : "task",
    localField : "_id",
    foreignField : "owner"
});





userSchema.pre("save", async function (next)  {
    const user = this;

    if(user.isModified("password"))
        user.password = await bcrypt.hash(user.password, 8)

    next();
})

userSchema.methods.generateAuthToken = async function() {

    const user = this;


    const token = jwt.sign({_id:user._id.toString()}, secret_key);
      user.tokens = user.tokens.concat({token});

    await user.save();
    return token;


}


userSchema.methods.toJSON =   function () {
  const user = this;
  const userObject = user.toObject();
  delete userObject.password;
  delete userObject.tokens;
  delete userObject.__v;

  return userObject;

};



userSchema.statics.findOneByCredentials = async function (email, password) {
//    console.log("ERRRRRRRR")
    const user = await User.findOne({email});

    if(!user)
        throw new Error("Incorrect email");

    const isMatch = await bcrypt.compare(password, user.password);

    if(!isMatch)
        throw new Error("Incorrect password");

    return user;


};


//Create Model
const User = mongoose.model("User", userSchema);



//Module Export
module.exports = User;