const mapbox_access_token = "pk.eyJ1IjoibWVyY3VyeXMiLCJhIjoiY2ttZWhzamYzMG16NTJzcDVwY3o0Z2VwYSJ9.4WKniG2rHYQ4NRtQsodpGA";
const mapbox = require('mapbox');
let client = new mapbox(mapbox_access_token);


exports.getMapCord = (city, code) => client
    .geocodeForward(`${city}, ${code}`)
    .then(res => {
            let cord = res
                .entity
                .features
                .find( element => element.matching_place_name === `${city}, ${code}`)
                .geometry
                .coordinates;
            return {lat : cord[1], lon  : cord[0]};

        }
    )
    .catch(err => console.log(err));

