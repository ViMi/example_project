const validator = require('validator');

module.exports =  value => validator.isEmail(value);