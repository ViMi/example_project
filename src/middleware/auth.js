const jwt = require("jsonwebtoken");
const model = require("./../models/User");
const secret_key = "kdweueksdsjkij";


module.exports = async (req, res, next) => {
    try{


        const token = req.header("Authorization").replace("Bearer ", "");
        const decoded = jwt.verify(token, secret_key);
        const user = await model.findOne({_id:decoded._id, "tokens.token": token});
        if(!user)
            throw new Error("");

        req.user = user;
        req.token = token;
       next();

     //   return req;

    } catch (e) {
        res.status(401).send({error: "Please authenticate"})
    }

};;