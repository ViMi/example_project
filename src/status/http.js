const OK = 200;
const CREATE = 201;
const NO_CONTENT = 204;


const UNAUTHORIZED = 401;
const FORBIDDEN = 403;
const NOT_FOUND = 404;

const INTERNAL_SERVER_ERROR = 500;


module.exports.OK = OK;
module.exports.CREATE = CREATE;
module.exports.NO_CONTENT = NO_CONTENT;
module.exports.UNAUTHORIZED = UNAUTHORIZED;
module.exports.FORBIDDEN = FORBIDDEN;
module.exports.NOT_FOUND = NOT_FOUND;
module.exports.INTERNAL_SERVER_ERROR = INTERNAL_SERVER_ERROR;