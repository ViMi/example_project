const http_status = require("../../status/http");

module.exports = (req, res, model) => {
    model.find({})
        .then( users => res.status(http_status.OK).send(users))
       .catch( err => res.status(http_status.INTERNAL_SERVER_ERROR).send(err.message));
}