
const jwt = require("jsonwebtoken");
const secret_key = "kdweueksdsjkij";

module.exports = async (req, res, model) =>{

  try{

    const token = req.header("Authorization").replace("Bearer ", "");

    const decoded = jwt.verify(token, secret_key);
    let user = await model
        .findOne({_id:decoded._id, "tokens.token": token})
        .populate("tasks");

    console.log(user);

    if(!user)
      throw new Error("");

    req.user = user;
    req.token = token;



    res.send(req.user)
    //next();

  } catch (e) {
    res.status(401).send({error: "Please authenticate"})
  }


 // res.send(req.user)
}