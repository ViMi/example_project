const getIdFromRequest = require("../../utils/getIdFromRequest");
const http_status = require("../../status/http");

module.exports = async (req, res, model) => {

    const findModelItem = await model.findById(getIdFromRequest(req));
    const updates = ["name", "email", "password", "age"];
    updates.forEach(update => findModelItem[update] = req.body[update]);

    findModelItem.save()
        .then(_ => res.status(http_status.OK).send(true))
        .catch(err => res.status(http_status.INTERNAL_SERVER_ERROR).send(err.message));


}
