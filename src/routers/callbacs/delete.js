const getIdFromRequest = require("../../utils/getIdFromRequest");
const http_status = require("../../status/http");


module.exports = (req, res, model) => {
    model.deleteOne({_id: getIdFromRequest(req)})
        .then( _ => res.status(http_status.OK).send(true))
        .catch(err => res.status(http_status.INTERNAL_SERVER_ERROR).send(err.message));

}