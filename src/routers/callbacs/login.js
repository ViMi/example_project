const http_status = require("../../status/http");

module.exports = async (req, res, model) => {
try{
    const findModel = await model.findOneByCredentials(req.body.email, req.body.password);
    const token = await findModel.generateAuthToken();
    //console.log(findModel);
    //console.log(token);
    res.send({findModel, token});
} catch (e)  {
    res.status(400).send(e.message);
}

};