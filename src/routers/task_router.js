//Models Includes
const express = require("express");
const Task = require("../models/Task.js");

const auth = require("../middleware/auth");
const jwt = require("jsonwebtoken");
const model = require("./../models/User");
const secret_key = "kdweueksdsjkij";
const getIdFromRequest = require("../utils/getIdFromRequest");

//!-----

//Work Var`s init
const router = express.Router();
const delete_callback = require("../routers/callbacs/delete");
const get_callback = require("../routers/callbacs/get");
const get_id_callback = require("../routers/callbacs/get_by_id");
const post = require("../routers/callbacs/post");
const put = require("../routers/callbacs/put");
//Main Logic



router
    .get("/tasks", auth, async (req, res) =>
{
    try{
        const task = await Task.find({owner:req.user.id});
        console.log(task);
        if(task)
            res.send(task);
        else
            res.send(false).status(404);
    } catch(e){
        res.send(e.message).status(404);
    }


})
    .get("/tasks(/:id)?", auth,async(req, res) =>
{
    try{
        const task = Task.findOne({owner:req.user.id, _id:getIdFromRequest(req)});
        if(task)
            res.send(task);
        else res.send("Error Of Task Index").status(404);

    } catch(e){
        res.send(e.message).status(404);
    }

})
    .post("/tasks", auth, async(req,res) => {

        try {
            const task = new Task({
                ...req.body,
                owner: req.user._id
            });

            await task.save();
            res.status(201).send(task);
        } catch (e) {
            res.status(500).send(e);
        }
    })
    .put("/tasks(/:id)?",auth,async (req, res) =>
    {
        try{
            const task = await Task.findOne({owner:req.user.id, _id:getIdFromRequest(req)});
            if(task){
                const updates = ["description", "completed"];
                updates.forEach(update => task[update] = req.body[update]);
                res.send(task);
            }

        } catch(e){
            res.send(e.message).status(404);
        }

        //Only owner
    });
//!----


//Module Export
module.exports = router;
