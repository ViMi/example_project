//Models Includes
const express = require("express");
 require("../db/mongoose.js");

 const Task = require("../models/Task");
const User = require("../models/User.js");

const auth = require("../middleware/auth");

const jwt = require("jsonwebtoken");
const secret_key = "kdweueksdsjkij";

//!-----


//Work Var`s init
const router = express.Router();
const delete_callback = require("../routers/callbacs/delete");
const get_callback = require("../routers/callbacs/get");
const get_id_callback = require("../routers/callbacs/get_by_id");
const post = require("../routers/callbacs/post");
const put = require("../routers/callbacs/put");
const login = require("../routers/callbacs/login")
const me = require("../routers/callbacs/me");
const logout = require("../routers/callbacs/logout");
//!----



//Main Logic


router
    .get("/users", (req, res) => get_callback(req,res, User))
    .get("/users/me", auth, async (req, res) => {

        await me(req, res, User)

    })
    .get("/users(/:id)?", (req, res) => get_id_callback(req,res, User))
    .post("/users", (req, res) => post(req,res, User))
    .post("/users/logout", auth, (req, res) => logout(req,res, User))
   .post("/users/logoutAll", auth, async (req,res) => {

       try{
           req.user.tokens = [];
           await req.user.save();
           res.status(200).send("Logout from all devices");
       } catch (e) {
           res.status(401).send(e.message)
       }

    })
    .post("/users/login", (req, res) => login(req,res, User))
    .delete("/users/me", auth, async (req,res) => {
        try{
            await req.user.remove();
            res.send(req.user);
        } catch(e) {
            res.status(500).send(e.message);
        }
    })
    .delete("/users(/:id)?", (req, res) => delete_callback(req,res, User))
    .put("/users/me", auth,async(req, res) => {
        try{
            const updates = ["name", "email", "password", "age"];
            updates.forEach(update => user[update] = req.body[update]);

            res.send(user);

        } catch(e) {
            res.status(500).send(e.message);
        }
    })
    .put("/users(/:id)?", (req, res) => put(req,res, User));

//!----


//Module Export
module.exports = router;
