import api from "./Api";

export default {
    fetchTasks() {
        return api().get("/tasks");
    },

    addTask(params) {
        return api.post("/tasks", params);
    },

    getTask(params) {
        return api.get("/tasks/"+ params.id);
    },

    updateTask(params){
        return api().patch("/tasks/"+params.id, params);
    },

    deleteTasks(params) {
        return api().delete("/tasks/"+params.id);
    }
}