//npm i axios --- allow make all types of request from client to server

import axios from "axios";

export default () => {
    return axios.create({
        baseURL:"http://localhost:8081"
        })
}
