//Modules Include

require("./src/db/mongoose.js");
require('./src/models/User');
require('./src/models/Task');

const express = require("express");
const config = require("./src/config/global");
const view_config = require("./src/config/views")

const hbs = require("hbs");
const user_router = require("./src/routers/user_router.js");
const task_router = require("./src/routers/task_router.js");
const app_router = require("./src/routers/app_router.js");

//!-----


//Create work variables
const app = express();
//!------


//Init static Files
app.use(config.CSS_VPATH, express.static(config.CSS_PATH));
app.use(config.JS_SCRIPTS_VPATH,express.static(config.JS_SCRIPTS_PATH));
//!-------

hbs.registerPartials(config.VIEW_PATH);
app.set("view engine", view_config.VIEW_ENGINE);


//App Code
app.use(express.json());

app.use(task_router);
app.use(user_router);
app.use(app_router);

app.listen(config.PORT);



